import { createRoot } from 'react-dom/client';
import Application  from './Application';

const rootEl = document.getElementById('root');
const root = createRoot(rootEl);
root.render(<Application />);

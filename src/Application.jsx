import React, { useState } from "react";
import {
  Box,
  createTheme,
  Grid,
  ThemeProvider,
  Typography,
  Stack,
  Slider,
} from "@mui/material";

import { grey, indigo, blue, red } from "@mui/material/colors";
const theme = createTheme({
  typography: {
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    fontSize: 12,
  },
});

const random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;
const arrayRandom = (array) => array[random(0, array.length - 1)];
const operations = ["+", "-"];
const statement = (min, max) => {
  let op = arrayRandom(operations);
  let a = random(min, max);
  // make sure a - b >= 0
  let b = op === "-" ? random(min, a) : random(a, max);
  let result = eval(`${a} ${op} ${b}`);
  return [a, op, b, result];
};

const generate = (min, max) => {
  let [a, op, b, result] = statement(min, max);
  let lastOp =
    result > 0
      ? arrayRandom(operations)
      : arrayRandom(operations.filter((op) => op !== "-"));
  let c = lastOp === "-" ? random(min, result) : random(min, max);
  return [a, op, b, lastOp, c, eval(`${result} ${lastOp} ${c}`)];
};

const display = ([a, op, b, lastOp, c, result]) => {
  let arr = [a, op, b, lastOp, c, "=", result];
  let hidden = arrayRandom([a, b, c]);
  arr[arr.indexOf(hidden)] = "( )";
  let pos = random(0);
  arr[pos] = "()";
  return arr.join(" ");
};

const Item = ({ min = 1, max = 20 }) => {
  return (
    <Box
      sx={{
        bgcolor: grey[50],
        borderRadius: 1,
        padding: 1,
        "&:hover": {
          bgcolor: grey[200],
          color: indigo[600],
        },
      }}
    >
      <Typography variant="body1" fontSize={"14"}>
        {display(generate(min, max))}
      </Typography>
    </Box>
  );
};

export default () => {
  const [num, setNum] = useState(10);
  const [numberRange, setNumberRange] = useState([1, 10]);
  return (
    <ThemeProvider theme={theme}>
      <Box padding={1}>
        <Stack spacing={2}>
          <Box>
            <Typography variant="h6">题目数量：{num}</Typography>
            <Slider
              defaultValue={num}
              value={num}
              min={1}
              max={100}
              onChange={(e) => setNum(e.target.value)}
              valueLabelDisplay="auto"
            />
          </Box>
          <Box>
            <Typography variant="h6">
              数字区间：[{numberRange[0]},{numberRange[1]}]
            </Typography>
            <Slider
              min={1}
              max={99}
              valueLabelDisplay="auto"
              defaultValue={numberRange}
              value={numberRange}
              onChange={(_, values) => {
                setNumberRange(values);
              }}
            />
          </Box>
        </Stack>
        <Grid container spacing={1} alignItems="center">
          {Array.from({ length: num }).map((_, i) => {
            return (
              <Grid key={i} item xs={4} md={2}>
                <Item min={numberRange[0]} max={numberRange[1]} />
              </Grid>
            );
          })}
        </Grid>
      </Box>
    </ThemeProvider>
  );
};
